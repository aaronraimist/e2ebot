#!/usr/bin/env python3
from matrix_client.client import MatrixClient
from matrix_client.crypto.olm_device import OlmDevice
import json

with open("config.json", "r") as f:
    config = json.load(f)

client = MatrixClient(config['homeserver'], encryption=True)#, token=config['token'], user_id=config['user_id'], restore_device_id=True, device_id=config['device_id'])

# work around a shortcoming in the E2E support, can't automatically create a MatrixClient with an access token
client.user_id = config['user_id']
client.token = config['token']
client.api.token = client.token
client.device_id = config['device_id']

if not client.olm_device:
    client.olm_device = OlmDevice(
        client.api, client.user_id, client.device_id, **client.encryption_conf)
client.olm_device.upload_identity_keys()
client.olm_device.upload_one_time_keys()

client.sync_filter = '{ "room": { "timeline" : { "limit" : 10 } } }'
client._sync()
# done loading

client.verify_devices = False

print(client.get_rooms())

def event_listener(room, event):
    if event['sender'] != client.user_id:
        #if event['type'] == 'm.room.encrypted':
        if event['type'] == 'm.room.message' and event['content']['msgtype'] == 'm.text':
            if event['content']['body'] == "!leave" and event['sender'] == config['owner']:
                room.leave()
                return
            elif event['content']['body'] == "!ping":
                room.send_notice("Pong!")
        print("{}: {}".format(room, event))
    
def invite_listener(room_id, state):
    client.join_room(room_id)
    client.get_rooms()[room_id].add_listener(event_listener)

client.add_invite_listener(invite_listener)

for id, room in client.get_rooms().items():
    room.add_listener(event_listener)

client.listen_forever()