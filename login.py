#!/usr/bin/env python3
from matrix_client.client import MatrixClient
import getpass
import json

homeserver=input("Homeserver URL: ")
username=input("User ID: ")
password=getpass.getpass("Password: ")
owner=input("Your Matrix ID (for giving admin commands to the bot): ")

client = MatrixClient(homeserver, encryption=True)

token = client.login(username=username, password=password, sync=False)

with open("config.json", "w") as f:
    json.dump({"homeserver":homeserver, "user_id":username, "device_id":client.device_id, "token":token, "owner":owner}, f)
